from core.db.session import Base, db_session

__all__ = ["Base", "session"]

import fastapi as _fastapi
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from fastapi_login import LoginManager
from fastapi_login.exceptions import InvalidCredentialsException

from app.v1.user.routers import users
from app.v1.blog.routers import blog
from app.v1.user.services import services as _services
from app.v1.user.models import models as _models
from app.v1.user.schemas import schemas as _schemas

app = _fastapi.FastAPI()

app.include_router(users.router)
app.include_router(blog.router)

SECRET = "super-secret-key"
manager = LoginManager(SECRET, "/login")

DB = {
    'users': {
        'johndoe@mail.com': {
            'name': 'John Doe',
            'password': 'hunter2'
        }
    }
}

@manager.user_loader()
def query_user(useremail: str, db: "Session" = _fastapi.Depends(_services.get_db)):
    user = _services.get_user_detail_by_mail(useremail=useremail, db=db)
    return user

@app.post('/login')
def login(data: OAuth2PasswordRequestForm = _fastapi.Depends(), db: "Session" = _fastapi.Depends(_services.get_db)):
    email = data.username
    password = data.password

    user = query_user(email)
    if not user:
        raise InvalidCredentialsException
    # if not _services.verify_password(user=user, password=password, db=db):
    #     raise InvalidCredentialsException
    return user
import datetime as _dt
from pydantic import BaseModel

class _BaseUser(BaseModel):
    firstname: str
    lastname: str
    username: str
    email: str
    hashed_password: str
    class Config:
        schema_extra = {
            "example": {
                "firstname": "Chandan",
                "lastname": "Sharma",
                "username": "chandan",
                "email": "chandan@gmail.com",
                "hashed_password": "chandan"
            }
        }

class User(_BaseUser):
    email: str
    date_created: _dt.datetime
    class Config:
        orm_mode = True
class CreateUser(_BaseUser):
    pass
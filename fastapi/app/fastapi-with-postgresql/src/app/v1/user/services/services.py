from typing import TYPE_CHECKING, List

from fastapi import Depends, HTTPException, status
# from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

import core.db.database as _database
from ..models import models as _models
from ..schemas import schemas as _schemas

if TYPE_CHECKING:
    from sqlalchemy.orm import Session

def get_db():
    db = _database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

# oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def _add_tables():
    return _database.Base.metadata.create_all(bind=_database.engine)

def search_user(user: _schemas.CreateUser, db: "Session" = Depends(get_db())) -> bool:
    _user = db.query(_models.User).filter(_models.User.email == user.email).first()
    if _user:
        return True
    return False

async def create_user(user: _schemas.CreateUser, db: "Session" = Depends(get_db())) -> _schemas.User:
    user = _models.User(**user.dict())
    if search_user(user=user, db=db):
        raise HTTPException(status_code=status.HTTP_302_FOUND, detail="User already exists")
    db.add(user) 
    db.commit()
    db.refresh(user)
    return _schemas.User.from_orm(user)

async def get_users(db: "Session" = Depends(get_db())) -> List[_schemas.User]:
    users = db.query(_models.User).all()
    return list(map(_schemas.User.from_orm, users))

async def get_user_detail_by_mail(useremail: str, db: "Session" = Depends(get_db())) -> _schemas.User:
    user = db.query(_models.User).filter(_models.User.email == useremail).first()
    return _schemas.User.from_orm(user)

async def delete_user(useremail: str, db: "Session" = Depends(get_db())) -> bool:
    user = get_user_detail_by_mail(useremail=useremail, db=db)
    if not db.delete(user):
        raise "User not deleted"
    db.commit()
    if not user:
        return False
    return True

async def update_user(user_data: _schemas.CreateUser, user: _models.User, db: "Session" = Depends(get_db())) -> _schemas.User:
    user.firstname = user_data.firstname
    user.lastname = user_data.lastname
    user.email = user_data.email

    db.commit()
    db.refresh(user)

    return _schemas.User.from_orm(user)

def authenticate_user(useremail: str, password: str, db: "Session" = Depends(get_db())):
    user = get_user_detail_by_mail(useremail=useremail, db=db)
    if not user:
        return False
    # if not verify_password(password, user.hashed_password):
    #     return False
    return user

def verify_password(user: _models.User, password: str, db: "Session" = Depends(get_db())):
    if user.hashed_password == password:
        return True
    return False

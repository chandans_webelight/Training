from typing import List
import fastapi as _fastapi
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
import datetime as _dt

from app.v1.user.schemas import schemas as _schemas
from app.v1.user.services import services as _services

router = _fastapi.APIRouter()

@router.post("/create-table/")
async def create_table(db: Session = _fastapi.Depends(_services.get_db)):
    _services._add_tables()

# @router.get("/api/users/me")
# async def read_users_me(current_user: _schemas.User = _fastapi.Depends(_services.get_current_active_user)):
#     return current_user

@router.post("/api/users/", response_model=_schemas.User, tags=["users"])
async def create_user(user: _schemas.CreateUser, db: "Session" = _fastapi.Depends(_services.get_db)):
    return await _services.create_user(user=user, db=db)

@router.post("/api/users/", response_model=_schemas.User)
async def create_user(user: _schemas.CreateUser, db: "Session" = _fastapi.Depends(_services.get_db)):
    return await _services.create_user(user=user, db=db)

@router.get("/api/users/", response_model=List[_schemas.User])
async def get_users(db: "Session" = _fastapi.Depends(_services.get_db)):
    return await _services.get_users(db=db)

@router.get("/api/users/{user_id}/", response_model=_schemas.User)
async def get_user(useremail: str, db: "Session" = _fastapi.Depends(_services.get_db)):
    user = await _services.get_user_detail_by_mail(useremail=useremail, db=db)
    if user is None:
        raise _fastapi.HTTPException(status_code=404, detail="User not found")
    
    return user

@router.delete("/api/users/{user_id}/")
async def delete_user(useremail: str, db: "Session" = _fastapi.Depends(_services.get_db)):
    # user = _services.get_user_detail_by_mail(useremail=useremail, db=db)
    # if user is None:
    #     raise _fastapi.HTTPException(status_code=404, detail="User not found")
    
    if not await _services.delete_user(useremail=useremail, db=db):
        raise _fastapi.HTTPException(status_code=_fastapi.status.HTTP_304_NOT_MODIFIED, detail="user not deleted")

    return {"Delete Status": "Success"}

@router.put("/api/users/{useremail}/", response_model=_schemas.User)
async def update_user(useremail: str, user_data: _schemas.CreateUser, db: "Session" = _fastapi.Depends(_services.get_db)) -> _schemas.User:
    user = await _services.get_user_detail_by_mail(useremail=useremail, db=db)
    if user is None:
        raise _fastapi.HTTPException(status_code=404, detail="User not found")
    
    user_bool = await _services.update_user(user_data=user_data, user=user, db=db)
    if user_bool is None:
        raise _fastapi.HTTPException(status_code=_fastapi.status.HTTP_304_NOT_MODIFIED, detail="user not updated")
    
    return user

# @router.post("/token")
# async def login(form_data: OAuth2PasswordRequestForm = _fastapi.Depends(), db: "Session" = _fastapi.Depends(_services.get_db)) -> _schemas.User:
#     return await _services.login(form_data=form_data, db=db)



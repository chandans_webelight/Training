import datetime as _dt
import sqlalchemy as _sql
import sqlalchemy.orm as _orm

from core.db.database import Base
class User(Base):
    __tablename__ = "users"

    id = _sql.Column(_sql.Integer, primary_key = True, index=True)
    firstname = _sql.Column(_sql.String, index=True)
    lastname = _sql.Column(_sql.String, index=True)
    username = _sql.Column(_sql.String, index=True)
    email = _sql.Column(_sql.String, index=True, unique=True)
    hashed_password = _sql.Column(_sql.String)
    blogs = _orm.relationship("Blog", back_populates="owner")
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
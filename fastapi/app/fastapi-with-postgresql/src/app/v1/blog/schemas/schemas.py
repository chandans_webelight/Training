import datetime as _dt
from pydantic import BaseModel

class _BaseBlog(BaseModel):
    blog_title: str
    blog_tag: str
    blog_body: str

    class Config:
        schema_extra = {
            "example": {
                "blog_title": "My First Blog",
                "blog_tag": "Tourism",
                "blog_body": "Into the Himalayas"
            }
        }

class Blog(_BaseBlog):
    id: int
    date_created: _dt.datetime

    class Config:
        orm_mode = True

class CreateBlog(_BaseBlog):
    pass
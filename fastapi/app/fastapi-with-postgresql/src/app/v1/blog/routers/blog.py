from typing import List

from fastapi import Depends, APIRouter, HTTPException, status
from sqlalchemy.orm import Session

from dependencies import get_token_header
from ..schemas import schemas as _schemas
from ..services import services as _services

router = APIRouter(
    prefix="/blogs",
    tags=["blogs"],
    dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not Found"}}
)

@router.post("/api/blogs/")
async def create_blog(blog: _schemas.CreateBlog, db: "Session" = Depends(_services.get_db)):
    return await _services.create_blog(blog=blog, db=db)

@router.get("/api/blogs/", response_model=List[_schemas.Blog])
async def get_blogs(db: "Session" = Depends(_services.get_db)):
    return await _services.get_blogs(db=db)

@router.get("/api/blogs/{blog_id}/", response_model=_schemas.Blog)
async def get_blog(blog_id: int, db: "Session" = Depends(_services.get_db)):
    blog = await _services.get_blog_by_id(blog_id=blog_id, db=db)
    if blog is None:
        raise HTTPException(status_code=404, detail="Blog not found")
    return blog

@router.delete("/api/blogs/{blog_id}/")
async def delete_blog(blog_id: int, db: "Session" = Depends(_services.get_db)):
    blog = await _services.get_blog_by_id(blog_id=blog_id, db=db)
    if blog is None:
        raise HTTPException(status_code=404, detail="Blog not found by the id: " + blog_id)
    
    blog_bool = await _services.delete_blog(blog=blog, db=db)
    if blog_bool:
        raise HTTPException(status_code=status.HTTP_304_NOT_MODIFIED, detail="failed to delete blog")

    return {"Delete Status": "Success"}

@router.put("/api/blogs/{blog_id}/", response_model=_schemas.Blog)
async def update_blog(blog_id: int, blog_data: _schemas.CreateBlog, db: "Session" = Depends(_services.get_db)) -> _schemas.Blog:
    blog = await _services.get_blog_by_id(blog_id=blog_id, db=db)
    if blog is None:
        raise HTTPException(status_code=404, detail="Blog not found")
    
    blog_bool = await _services.update_blog(blog_data=blog_data, blog=blog, db=db)
    if blog_bool is None:
        raise HTTPException(status_code=status.HTTP_304_NOT_MODIFIED, detail="user not updated")
    
    return blog
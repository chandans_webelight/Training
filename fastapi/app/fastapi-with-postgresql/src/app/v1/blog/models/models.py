import datetime as _dt
import sqlalchemy as _sql
import sqlalchemy.orm as _orm

from core.db.database import Base

class Blog(Base):
    __tablename__ = "blogs"

    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    blog_title = _sql.Column(_sql.String, index=True)
    blog_tag = _sql.Column(_sql.String, index=True)
    blog_body = _sql.Column(_sql.String, index=True)
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
    owner_id = _sql.Column(_sql.Integer, _sql.ForeignKey("users.id"))

    owner = _orm.relationship("User", back_populates="blogs")
from typing import List

from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session

import core.db.database as _database
from ..models import models as _models
from ..schemas import schemas as _schemas

def get_db():
    db = _database.SessionLocal()
    try:
        yield db
    finally:
        db.close()

async def create_blog(blog: _schemas.CreateBlog, db: "Session" = Depends(get_db)):
    blog = _models.Blog(**blog.dict())
    db.add(blog)
    db.commit()
    db.refresh(blog)
    return _schemas.Blog.from_orm(blog)

async def get_blogs(db: "Session" = Depends(get_db())) -> List[_schemas.Blog]:
    blogs = db.query(_models.Blog).all()
    return list(map(_schemas.Blog.from_orm, blogs))

async def get_blog_by_id(blog_id: int, db: "Session" = Depends(get_db())) -> _schemas.Blog:
    blog = db.query(_models.Blog).filter(_models.Blog.id == blog_id).first()
    return _schemas.Blog.from_orm(blog)

async def delete_blog(blog: _models.Blog, db: "Session" = Depends(get_db())):
    db.delete(blog)
    db.commit()

async def update_blog(blog_data: _schemas.CreateBlog, blog: _models.Blog, db: "Session" = Depends(get_db())) -> _schemas.Blog:
    blog.blog_title = blog_data.blog_title
    blog.blog_tag = blog_data.blog_tag
    blog.blog_body = blog_data.blog_body

    db.commit()
    db.refresh(blog)

    return _schemas.Blog.from_orm(blog)
from fastapi import Header, HTTPException

async def get_token_header(x_token: str = Header()):
    if x_token != "secret":
        raise HTTPException(status_code=400, detail="X-Token is invalid")
    
async def get_query_token(token: str):
    if token != "chandan":
        raise HTTPException(status_code=400, detail="No Jessica Token found")